1. Abre este repositorio y copia la URL para clonar: <https://bitbucket.org/git-valencia-2/random-name> . Clónalo en tu máquina.
2. Localiza el commit de la versión 0.1.0 y crea una rama a partir de él. Llámala develop-tunombre y muévete hasta la nueva rama.
3. Crea otra nueva rama, llámala bugfix-utf8-tunombre y muévete hasta ella.
4. Corrige el caracter extraño de Amélie en la línea 820 del archivo names.txt y guárdalo en
un commit.
5. Crea una nueva rama llamada spanish-names-tunombre.
6. Introduce varios nombres en el archivo names.txt y haz un commit por cada uno que añadas.
7. Mergea en develop-tunombre todos los cambios que has hecho en tus dos ramas.
8. Borra las dos ramas anteriores.
9. Crea una nueva rama llamada chinese-names-tunombre a partir del commit de la versión 0.1.1.
10. Aplícale la corrección que hiciste en bugfix-utf8-tunombre a esta rama (no hay que corregir el nombre a mano, sino aplicar el cambio mediante algún comando de git).
11. Mergea esa nueva rama en develop-tunombre.
12. Comprueba si en el repositorio remoto existe ya una rama con la misma que develop-tunombre.
13. Si existe, renombra la tuya y envíala al repositorio remoto.
14. Si no existe, envía la rama develop-tunombre al repositorio remoto.
